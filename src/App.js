import * as React from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';

import Context from './components/drop_down/Context';
import MeasurementSystem from './components/drop_down/MeasurementSystem'
import Unit from './components/drop_down/Unit';
import Number from './Number';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Area', 159, 6.0, 24),
];

export default function App() {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <Context />
        <MeasurementSystem />
        <Unit />
        <Number />

        <Stack spacing={2} height={57} direction="row">
          <Button variant="contained">Convert</Button>
        </Stack>

        {/* <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>Context</TableCell>
                <TableCell align="right">Acre</TableCell>
                <TableCell align="right">M2</TableCell>
                <TableCell align="right">Rai(ไร่)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.calories}</TableCell>
                  <TableCell align="right">{row.fat}</TableCell>
                  <TableCell align="right">{row.carbs}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer> */}
       </div>

      );
}

