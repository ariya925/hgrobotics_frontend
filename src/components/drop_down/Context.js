import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const Context = () => {
    const [context, setConText] = React.useState('');

    const getContext = (event) => {
        setConText(event.target.value)
    }

    return (
        <div className="App">

            <FormControl sx={{ m: 1, minWidth: 100 }}>
                <InputLabel id="demo-simple-select-autowidth-label">Context</InputLabel>
                <Select
                    labelId="demo-simple-select-autowidth-label"
                    id="demo-simple-select-autowidth"
                    value={context}
                    onChange={getContext}
                    autoWidth
                    label="Context"
                >
                    <MenuItem value='area'>Area</MenuItem>
                    <MenuItem value='distance'>Distance</MenuItem>
                    <MenuItem value='volume'>Volume</MenuItem>
                    <MenuItem value='speed'>Speed</MenuItem>
                </Select>
            </FormControl>


        </div>
    )
}

export default Context;
