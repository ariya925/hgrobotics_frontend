import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import ListSubheader from '@mui/material/ListSubheader';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function Unit() {
    return (
        <div>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
                <InputLabel htmlFor="grouped-select">Untis</InputLabel>
                <Select defaultValue="" id="grouped-select" label="Grouping">

                    <ListSubheader>Context Area</ListSubheader>
                    <MenuItem value='acre'>acre</MenuItem>
                    <MenuItem value='m2'>m2</MenuItem>
                    <MenuItem value='rai'>rai(ไร่)</MenuItem>
                    
                    <ListSubheader>Context Distance</ListSubheader>
                    <MenuItem value='mile'>mile</MenuItem>
                    <MenuItem value='metre'>metre</MenuItem>
                    <MenuItem value='wa'>wa</MenuItem>

                    <ListSubheader>Context Volume</ListSubheader>
                    <MenuItem value='fl oz'>fl oz</MenuItem>
                    <MenuItem value='cm3'>cm3</MenuItem>
                    <MenuItem value='ml'>ml</MenuItem>

                    <ListSubheader>Context Speed</ListSubheader>
                    <MenuItem value='mph'>mph</MenuItem>
                    <MenuItem value='m/s'>m/s</MenuItem>
                    <MenuItem value='km/h'>km/h</MenuItem>
                </Select>
            </FormControl>
        </div>
    );
}