import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const MeasurementSystem = () => {
    const [MeasurementSystem, setMeasurementSystem] = React.useState('');

    const getMeasurementSystem = (event) => {
        setMeasurementSystem(event.target.value)
    }

    return (
        <div className="App">

            <FormControl sx={{ m: 1, minWidth: 150 }}>
                <InputLabel id="demo-simple-select-autowidth-label">Measurement System</InputLabel>
                <Select
                    labelId="demo-simple-select-autowidth-label"
                    id="demo-simple-select-autowidth"
                    value={MeasurementSystem}
                    onChange={getMeasurementSystem}
                    autoWidth
                    label="Measurement System"
                >
                    <MenuItem value='imperial'>Imperial</MenuItem>
                    <MenuItem value='metric'>Metric</MenuItem>
                    <MenuItem value='customized'>Customized</MenuItem>
                </Select>
            </FormControl>


        </div>
    )
}

export default MeasurementSystem;
